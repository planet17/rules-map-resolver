### Rules Map Resolver ###

---

That library for helps and simplify development some logic of mapping between different parts of apps.

It can be used when app must have not direct mapping with some set options from one side and some set of options or just key for another.

Such like example you have following rules mapping. There is: array - condition, value after array - result what app must get.

        [
            'project' => 'project1',
            'language' => any,
            'platform' => desktop
        ]
            => 'type1',
        
        [
            'project' => 'project2',
            'language' => 'en',
            'platform' => mobile
        ]
            => 'type1',
        
        [
            'project' => 'project2',
            'language' => 'en',
            'platform' => desktop
        ]
            => 'type2',
        
        [
            'project' => any,
            'language' => any,
            'platform' => any
        ]
            => 'type3',


Requirements: 

---

        php >= 7.0

Example of uses:

```php
<?php
/* Implement your Rule implements RuleContract or try ConcreteRule like example. */
$rule     = new \Planet17\RulesMapResolver\Common\Example\ConcreteRule();
/* Implement your Map implements MapContract or try ConcreteMap for example. */
$map      = new \Planet17\RulesMapResolver\Common\Example\ConcreteMap($rule);
/* Use Resolver from package */
$resolver = new \Planet17\RulesMapResolver\Resolvers\Simple($map);
/* Construct array by options names described at class Rule. */
$current  = ['platform' => 'ios', 'language' => 'en', 'isDevEnv' => true, 'project' => 1];
/* Provide array to resolver it will find name of option what satisfy to your data whether it exist. */
echo 'Results:' . PHP_EOL . $resolver->resolve($current) . PHP_EOL;
?>
```
