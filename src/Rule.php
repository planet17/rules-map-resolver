<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */

namespace Planet17\RulesMapResolver;


use Planet17\RulesMapResolver\Contracts\RuleContract;
use Planet17\RulesMapResolver\Exceptions\Rules\{
    EmptyOptionsException,
    InvalidProvidedOptionsException,
    InvalidValueException,
    NonExistOptionException,
    NonUniqueOptionsException,
    NonUniqueValueException,
    OverflowRequiredOptionsException,
    RequiredOptionsDoesNotSetException
};


/**
 * Class Rule - it an base abstract class what contain almost all methods for
 * classes what will be extends that.
 *
 * Only two method must be implemented at child of it:
 *
 * ```
 * RuleContract::getOptsNames()
 * RuleContract::getRequiredOptsNames()
 * ```
 *
 * @package Planet17\RulesMapResolver
 */
abstract class Rule implements RuleContract
{
    /**
     * Assoc array. Key - options name. Each key is array with values what
     * had been set.
     *
     * @var array $opts
     * @see Rule::initPresetOpts()
     * @see Rule::addOptValue()
     * @see Rule::setOptValues()
     */
    private $opts;


    /**
     * Rule constructor.
     *
     * It call validation for both abstract method.
     *
     * And if all is ok then will be set keys from RuleContract::getOptsNames()
     * to the Rule::$opts.
     *
     * @see Rule::isValidPresetOpts() - Validate implementation of method.
     * @see Rule::initPresetOpts() - Prepare and set Rule::$opts.
     *
     * @throws EmptyOptionsException
     * @throws NonUniqueOptionsException
     * @throws OverflowRequiredOptionsException
     * @throws NonExistOptionException
     */
    final public function __construct()
    {
        $this->isValidPresetOpts();
        $this->initPresetOpts();
    }


    /**
     * Method validate implementation of methods:
     *
     * ```
     * RuleContract::getOptsNames(),
     * RuleContract::getRequiredOptsNames().
     * ```
     *
     * @throws EmptyOptionsException
     * @throws NonUniqueOptionsException
     * @throws OverflowRequiredOptionsException
     * @throws NonExistOptionException
     */
    final private function isValidPresetOpts()
    {
        $optsCount = \count($this->getOptsNames());
        if ($optsCount < 1) {
            throw new EmptyOptionsException();
        }

        if ($optsCount !== \count(array_unique($this->getOptsNames()))) {
            throw new NonUniqueOptionsException();
        }

        $requiredOptCount = \count($this->getRequiredOptsNames());
        if ($requiredOptCount !== \count(array_unique($this->getRequiredOptsNames()))) {
            throw new NonUniqueOptionsException();
        }

        if ($requiredOptCount > $optsCount) {
            throw new OverflowRequiredOptionsException();
        }

        $diff = array_diff($this->getRequiredOptsNames(), $this->getOptsNames());
        if ($diff) {
            throw new NonExistOptionException(serialize($diff));
        }
    }


    /**
     * Method prepare and set to Rule::$opts keys of options and empty array.
     */
    final private function initPresetOpts()
    {
        $this->opts = array_fill_keys($this->getOptsNames(), []);
    }


    /** @inheritdoc */
    abstract public function getOptsNames():array;


    /** @inheritdoc */
    abstract public function getRequiredOptsNames():array;


    /**
     * @inheritdoc
     * @return RuleContract|Rule
     *
     * @throws NonExistOptionException
     * @throws NonUniqueValueException
     * @throws InvalidValueException
     */
    final public function addOptValue(string $optName, $value):RuleContract
    {
        $this->isExistOption($optName);
        $this->isValidProvidedData([$value]);
        $this->isOptNotContainsValue($optName, $value);
        $this->opts[ $optName ][] = $value;

        return $this;
    }


    /**
     * @inheritdoc
     * @return RuleContract|Rule
     *
     * @throws NonExistOptionException
     * @throws NonUniqueValueException
     * @throws InvalidValueException
     */
    final public function setOptValues(string $optName, array $values):RuleContract
    {
        $this->isExistOption($optName);
        $this->isValidProvidedData($values);
        $this->opts[ $optName ] = array_values($values);

        return $this;
    }


    /**
     * Method checks if exist option name like a key at Rule::$opts.
     *
     * @param string $optName
     *
     * @throws NonExistOptionException
     */
    final private function isExistOption(string $optName)
    {
        if ( !array_key_exists($optName, $this->opts)) {
            throw new NonExistOptionException($optName);
        }
    }


    /**
     * Method validate values provided to some of methods:
     *
     * ```
     * Rule::addOptValue()
     * Rule::setOptValues()
     * ```
     *
     * From method Rule::addOptValue() from that method also validate
     * does provided value does not exists before.
     *
     * @param array   $values   - Provided array of values
     *
     * @throws NonUniqueValueException
     * @throws InvalidValueException
     *
     * @see Rule::addOptValue()
     * @see Rule::setOptValues()
     */
    final private function isValidProvidedData(array $values)
    {
        $valuesCount = \count($values);
        if ($valuesCount !== \count(array_unique($values))) {
            throw new NonUniqueValueException();
        }

        if (\count(array_filter($values, [$this, 'isValidValue'])) !== $valuesCount) {
            throw new InvalidValueException();
        }
    }


    /**
     * Method used for checking previous added values while append new values.
     *
     * @param string $optName  - Key of Option.
     * @param        $value    - Provided value.
     *
     * @throws NonUniqueValueException
     *
     * @see Rule::addOptValue()
     */
    final private function isOptNotContainsValue(string $optName, $value)
    {
        if (\count($this->opts[ $optName ]) && \in_array($value, $this->opts[ $optName ], true)) {
            throw new NonUniqueValueException();
        }
    }


    /**
     * @inheritdoc
     * @throws RequiredOptionsDoesNotSetException
     * @throws InvalidProvidedOptionsException
     * @throws InvalidValueException
     */
    public function match(array $data):int
    {
        $this->isCompleted();
        $this->isSuitProvidedData($data);

        $result = [];
        foreach ($this->getOptsNames() as $optName) {
            $result[] = $this->compareValue($optName, $data[ $optName ]);
        }

        return ($result && array_product($result));
    }


    /**
     * Method validate preset values before matching. It will checks
     * whether had been set values to required options.
     *
     * @throws RequiredOptionsDoesNotSetException
     */
    final private function isCompleted()
    {
        foreach ($this->getRequiredOptsNames() as $required) {
            if ( !$this->opts[ $required ]) {
                throw new RequiredOptionsDoesNotSetException();
            }
        }
    }


    /**
     * Method validate provided data to method RuleContract::match.
     * @param array $needle
     *
     * @see Rule::match()
     *
     * @throws InvalidProvidedOptionsException
     * @throws InvalidValueException
     */
    final private function isSuitProvidedData(array $needle)
    {
        if (\count($this->opts) !== \count($needle)) {
            throw new InvalidProvidedOptionsException();
        }

        if (array_diff($this->getOptsNames(), array_keys($needle))) {
            throw new InvalidProvidedOptionsException();
        }

        if (\count(array_filter($needle, [$this, 'isValidValue'])) !== \count($this->opts)) {
            throw new InvalidValueException();
        }
    }


    /**
     * Method filter wrong values.
     *
     * @param $item - Value for Option.
     *
     * @return boolean
     */
    final private function isValidValue($item):bool
    {
        return !\is_array($item);
    }


    /**
     * Method will compare provided `value` with `Option`.
     *
     * At first check if `Option` is not empty.
     * If `Option` is empty it means every value equals to it.
     *
     * If `Options` had some values - than provided will be search at that
     * list with strict mode (with checks types).
     *
     * When no matches found it will return true.
     * Otherwise it will return false.
     *
     * @param string $optName
     * @param        $value
     *
     * @return boolean
     */
    final private function compareValue(string $optName, $value):bool
    {
        /* is any value allowed */
        if ( !$this->opts[ $optName ]) {
            return true;
        }

        /* is provided value exist at Rule */
        if (\in_array($value, $this->opts[ $optName ], true)) {
            return true;
        }

        return false;
    }
}
