<?php
/**
 * Project: Rules Map Resolver
 * Author:  planet17
 */

namespace Planet17\RulesMapResolver\Exceptions;


/**
 * Class DuplicateKeyExceptions
 *
 * @package Planet17\RulesMapResolver\Exceptions
 */
class DuplicateRuleKeyExceptions extends \OutOfBoundsException
{
    public function __construct(string $name) {
        parent::__construct(
            'Attempting to add an existing key: ' . $name,
            0,
            null
        );
    }
}
