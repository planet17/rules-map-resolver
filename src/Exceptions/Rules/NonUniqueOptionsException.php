<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */

namespace Planet17\RulesMapResolver\Exceptions\Rules;


class NonUniqueOptionsException extends \LengthException
{
    public function __construct()
    {
        $message = 'Preset options names must be unique.';
        parent::__construct($message, 0, null);
    }
}
