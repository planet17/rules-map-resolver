<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */

namespace Planet17\RulesMapResolver\Exceptions\Rules;


class InvalidValueException extends \InvalidArgumentException
{
    public function __construct()
    {
        $message = 'Values can\'t be array.';
        parent::__construct($message, 0, null);
    }
}
