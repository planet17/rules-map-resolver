<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */

namespace Planet17\RulesMapResolver\Exceptions\Rules;


class EmptyOptionsException extends \LengthException
{
    public function __construct()
    {
        $message = 'Preset options names can\'t be empty.';
        parent::__construct($message, 0, null);
    }
}
