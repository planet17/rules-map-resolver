<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */


namespace Planet17\RulesMapResolver\Exceptions\Rules;


class NonExistOptionException extends \OutOfRangeException
{
    public function __construct(string $optNames)
    {
        $message = "Options names `{$optNames}` not exist at options.";
        parent::__construct($message, 0, null);
    }
}
