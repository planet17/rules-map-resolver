<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */

namespace Planet17\RulesMapResolver\Exceptions\Rules;


class InvalidProvidedOptionsException extends \LengthException
{
    public function __construct()
    {
        $message = 'Provided options must be the same like result of method' .
                   ' RuleContract::getOptsNames().';
        parent::__construct($message, 0, null);
    }
}
