<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */

namespace Planet17\RulesMapResolver\Exceptions\Rules;


class RequiredOptionsDoesNotSetException extends \UnexpectedValueException
{
    public function __construct()
    {
        $message = 'Must been set required options before any compare.';
        parent::__construct($message, 0, null);
    }
}
