<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */

namespace Planet17\RulesMapResolver\Exceptions\Rules;


class NonUniqueValueException extends \UnexpectedValueException
{
    public function __construct()
    {
        $message = 'Values at each option must been unique.';
        parent::__construct($message, 0, null);
    }
}
