<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */

namespace Planet17\RulesMapResolver\Exceptions\Rules;


class OverflowRequiredOptionsException extends \LengthException
{
    public function __construct()
    {
        $message = 'Required opts names list is bigger than list of all options names.';
        parent::__construct($message, 0, null);
    }
}
