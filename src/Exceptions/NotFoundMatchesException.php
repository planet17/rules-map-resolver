<?php
/**
 * Project: Rules Map Resolver
 * Author:  planet17
 */

namespace Planet17\RulesMapResolver\Exceptions;


class NotFoundMatchesException extends \LogicException
{
    public function __construct()
    {
        parent::__construct('Can\'t found any rules matching to provided data.', 0, null);
    }
}
