<?php
/**
 * Project: Rules Map Resolver
 * Author:  planet17
 */

namespace Planet17\RulesMapResolver\Contracts;


interface MapContract
{
    public function __construct(RuleContract $prototype);
    public function setUp();
    public function getPrototype();
    public function getRules();
    public function getRulesOptNames():array;
}
