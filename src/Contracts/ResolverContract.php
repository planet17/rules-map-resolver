<?php
/**
 * Project: Rules Map Resolver
 * Author:  planet17
 */

namespace Planet17\RulesMapResolver\Contracts;


interface ResolverContract
{
    public function __construct(MapContract $map);
    public function getRulesOptNames():array;
    public function resolve(array $needle):string;
}
