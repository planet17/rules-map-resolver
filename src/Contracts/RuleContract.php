<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */

namespace Planet17\RulesMapResolver\Contracts;


/**
 * Interface RuleContract.
 *
 * @package Planet17\RulesMapResolver\Contracts
 *
 * @see     RuleContract::getOptsNames() - Return allowed Options names
 * @see     RuleContract::getRequiredOptsNames() - Return Options names what required to set at least one value.
 * @see     RuleContract::addOptValue() - Append value to Options names.
 * @see     RuleContract::setOptValues() - Set list with values to Options names.
 * @see     RuleContract::match() - Matching provided data and preset. Then calculate all matches and return result.`
 */
interface RuleContract
{
    /**
     * That method must be implemented at domain side.
     *
     * Method return array of `options names` what can be set for matching.
     *
     * If any values will not be set by provided here options, that options
     * be empty. Empty values like mask for all possible variants. That options
     * will be satisfy to any provided values at RuleContract::match().
     *
     * @return array
     */
    public function getOptsNames():array;


    /**
     * That method must be implemented at domain side.
     *
     * Method return array of `required options names` what must be set for
     * matching. At least one value.
     *
     * Method just helps clearly understanding what opts does not match with
     * any value.
     *
     * @return array
     */
    public function getRequiredOptsNames():array;


    /**
     * Method append provided value to provided key.
     *
     * It implemented at base Rule class at library.
     *
     * @param string $optName - Key of allowed `options names`.
     *                        Key must exist at array returned by
     *                        RuleContract::getOptsNames().
     * @param        $value   - Any type of value with what will matching
     *                        provided data to RuleContract::match().
     *
     * @return RuleContract
     */
    public function addOptValue(string $optName, $value):RuleContract;


    /**
     * Method append provided value to provided key.
     *
     * It implemented at base Rule class at library.
     *
     * Caution: that methods will be override current values.
     *
     * @param string $optName - Key of allowed `options names`.
     *                        Key must exist at array returned by
     *                        RuleContract::getOptsNames().
     *
     * @param array  $values  - Array of any type of value with what
     *                        will be matching provided data to
     *                        RuleContract::match().
     *
     * @return RuleContract
     */
    public function setOptValues(string $optName, array $values):RuleContract;


    /**
     * Method will be match provided data to existing at object values.
     *
     * Caution: when value at key is empty, any provided data will be match
     * with it.
     *
     * @param array $data - Array with keys and values. Keys of provided data
     *                    must be same to `options names` returned by
     *                    RuleContract::getOptsNames().
     *
     * @return integer - Return `1` when all provided values match to object,
     *                 otherwise return `0`.
     */
    public function match(array $data):int;
}
