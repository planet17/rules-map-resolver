<?php
/**
 * Project: Rules Map Resolver
 * Author:  planet17
 */

namespace Planet17\RulesMapResolver\Resolvers;


use Planet17\RulesMapResolver\Contracts\MapContract;
use Planet17\RulesMapResolver\Contracts\ResolverContract;
use Planet17\RulesMapResolver\Contracts\RuleContract;
use Planet17\RulesMapResolver\Exceptions\NotFoundMatchesException;

class Simple implements ResolverContract
{
    protected $map;

    protected $needle;

    public function __construct(MapContract $map)
    {
        $this->map = $map;
        $this->map->setUp();
    }

    public function resolve(array $needle):string
    {
        /** @var RuleContract[] $map */
        $map = $this->map->getRules();
        /**
         * @var string       $name
         * @var RuleContract $item
         */
        foreach ($map as $name => $item) {
            if ($item->match($needle)) {
                return $name;
            }
        }

        throw new NotFoundMatchesException();
    }


    public function getRulesOptNames():array { return $this->map->getRulesOptNames(); }
}
