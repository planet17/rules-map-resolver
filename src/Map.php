<?php
/**
 * Project: Rules Map Resolver
 * Author:  planet17
 */

namespace Planet17\RulesMapResolver;


use Planet17\RulesMapResolver\Contracts\{MapContract, RuleContract};
use Planet17\RulesMapResolver\Exceptions\DuplicateRuleKeyExceptions;


/**
 * Class Map
 *
 * @package Planet17\RulesMapResolver
 */
abstract class Map implements MapContract
{
    private $prototype;

    private $rules = [];

    public function __construct(RuleContract $prototype)
    {
        $this->prototype = $prototype;
    }

    abstract public function setUp();

    final public function isContainsRules():bool { return \count($this->rules); }

    protected function addRule(string $name, RuleContract $rule)
    {
        if (array_key_exists($name, $this->rules)) {
            throw new DuplicateRuleKeyExceptions($name);
        }

        $this->rules[ $name ] = $rule;
    }

    /** @return RuleContract */
    final public function getPrototype():RuleContract { return clone $this->prototype; }

    final public function getRules():array { return $this->rules; }

    final public function getRulesOptNames():array
    {
        return $this->prototype->getOptsNames();
    }
}
