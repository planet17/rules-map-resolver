<?php
/**
 * Project: Rules Map Resolver
 * Author:  planet17
 */

namespace Planet17\RulesMapResolver\Common\Example;


use Planet17\RulesMapResolver\Map;


/**
 * Class ConcreteMap
 *
 * @method ConcreteRule getPrototype()
 *
 * @package Planet17\RulesMapResolver\Common\Example
 */
class ConcreteMap extends Map
{
    public function setUp()
    {
        $this->addRule('web', $this->getPrototype()->addPlatform('web')->addPlatform('webDesktop')->addPlatform('desktop'));
        $this->addRule('mobile', $this->getPrototype()->addPlatform('webMobile')->addPlatform('mobileSites'));
        $this->addRule('1_ios_prod', $this->getPrototype()->addProjectId(1)->addPlatform('ios')->addEnv(false));
        $this->addRule('1_ios_test', $this->getPrototype()->addProjectId(1)->addPlatform('ios')->addEnv(true));
        $this->addRule('1_android', $this->getPrototype()->addProjectId(1)->addPlatform('android'));
        $this->addRule('2_android', $this->getPrototype()->addProjectId(2)->addPlatform('android')->addEnv(false));
        $this->addRule('2_android_test', $this->getPrototype()->addProjectId(2)->addPlatform('android')->addEnv(true));
    }
}
