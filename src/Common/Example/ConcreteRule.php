<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */

namespace Planet17\RulesMapResolver\Common\Example;


use Planet17\RulesMapResolver\{
    Contracts\RuleContract,
    Rule
};

/**
 * Class ConcreteExampleRule
 *
 * @package Planet17\RulesMapResolver\Common
 */
class ConcreteRule extends Rule
{

    /**
     *
     */
    const OPT_NAME_PROJECTS = 'project';

    /**
     *
     */
    const OPT_NAME_PLATFORM = 'platform';

    /**
     *
     */
    const OPT_NAME_LANGUAGE = 'language';
    /**
     *
     */
    const OPT_NAME_ENV      = 'isDevEnv';


    /** @inheritdoc */
    public function getOptsNames():array
    {
        return [
            self::OPT_NAME_PROJECTS,
            self::OPT_NAME_PLATFORM,
            self::OPT_NAME_LANGUAGE,
            self::OPT_NAME_ENV
        ];
    }


    /** @inheritdoc */
    public function getRequiredOptsNames():array
    {
        return [];
    }

    /**
     * Method more secure append one of `Option` values.
     *
     * @param integer $value
     *
     * @return RuleContract|ConcreteRule
     */
    public function addProjectId(int $value):RuleContract
    {
        return $this->addOptValue(self::OPT_NAME_PROJECTS, $value);
    }


    /**
     * Method more secure append one of `Option` values.
     *
     * @param string $value
     *
     * @return RuleContract|ConcreteRule
     */
    public function addPlatform(string $value):RuleContract
    {
        return $this->addOptValue(self::OPT_NAME_PLATFORM, $value);
    }


    /**
     * Method more secure append one of `Option` values.
     *
     * @param string $value
     *
     * @return RuleContract|ConcreteRule
     */
    public function addLanguage(string $value):RuleContract
    {
        return $this->addOptValue(self::OPT_NAME_LANGUAGE, $value);
    }


    /**
     * Method more secure append one of `Option` values.
     *
     * @param boolean $isDev
     *
     * @return RuleContract|ConcreteRule
     */
    public function addEnv(bool $isDev):RuleContract
    {
        return $this->addOptValue(self::OPT_NAME_ENV, $isDev);
    }
}
