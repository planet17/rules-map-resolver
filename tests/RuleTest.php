<?php
/**
 * Project:     Rules Map Resolver
 * Author:      planet17
 */

namespace Planet17\RulesMapResolver;


use PHPUnit\Framework\{MockObject\MockObject, TestCase};
use Planet17\RulesMapResolver\Contracts\RuleContract;
use Planet17\RulesMapResolver\Exceptions\Rules\{
    EmptyOptionsException,
    InvalidValueException,
    NonExistOptionException,
    NonUniqueOptionsException,
    InvalidProvidedOptionsException,
    NonUniqueValueException,
    OverflowRequiredOptionsException,
    RequiredOptionsDoesNotSetException
};


/**
 * Class RuleTest
 *
 * @package Planet17\RulesMapResolver
 */
class RuleTest extends TestCase
{
    /**
     * @const string NAME_ABS_METHOD_GET_OPTS
     * @see RuleContract::getOptsNames()
     */
    const NAME_ABS_METHOD_GET_OPTS     = 'getOptsNames';

    /**
     * @const string NAME_ABS_METHOD_GET_OPTS
     * @see RuleContract::getRequiredOptsNames()
     */
    const NAME_ABS_METHOD_GET_REQUIRED = 'getRequiredOptsNames';

    /**
     * @const NAME_ABS_METHOD_GET_OPTS
     * @see Rule::isCompleted()
     */
    const NAME_METHOD_CHECK_COMPLETED = 'isCompleted';


    /**
     * Test Exception while contracts method Rule::getOptsNames() have no
     * options. Exception must be throw while Rule::__construct() works.
     *
     * At that implementation __construct() had been called early than can be
     * define abstract method.
     *
     * @covers \Planet17\RulesMapResolver\Rule::__construct()
     */
    public function testInvalidOptionsEmptyException()
    {
        $this->expectException(EmptyOptionsException::class);
        $this->getMockForAbstractClass(
            Rule::class,
            [],
            '',
            true,
            false,
            true,
            [self::NAME_ABS_METHOD_GET_OPTS, self::NAME_ABS_METHOD_GET_REQUIRED]
        );
    }


    /**
     * Test Exception while contracts method Rule::getOptsNames() have no
     * options. But at that time with disabled original constructor and replace
     * it by call two protected method.
     *
     * At that implementation of test clearly defined abstract methods what
     * will return empty array.
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::__construct()
     */
    public function testInvalidOptionsEmptyExceptionReflection()
    {
        $this->expectException(EmptyOptionsException::class);
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn([]);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn([]);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
    }


    /**
     * Test construct it must been well while contracts method Rule::getOptsNames()
     * have options.
     *
     * @dataProvider providerValidOpts
     *
     * @param array $optsNames - Array with opts names.
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::__construct()
     */
    public function testValidGetOptsNames(array $optsNames)
    {
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();

        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn($optsNames);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn([]);

        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);

        $this->assertSame($optsNames, $mock->{self::NAME_ABS_METHOD_GET_OPTS}());
    }


    /**
     * Provider.
     * Contain some variants with valid sets of options.
     *
     * @return array
     * @see RuleTest::testValidGetOptsNames()
     */
    public function providerValidOpts():array
    {
        return [
            'oneItem'   => [['opt']],
            'twoItem'   => [['opt1', 'opt2']],
            'threeItem' => [['opt1', 'opt2', 'optionalOneMore']],
        ];
    }


    /**
     * Test Exception while contracts method Rule::getOptsNames() have invalid
     * options names set. It contains duplicate.
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::__construct()
     */
    public function testInvalidGetOptsNamesNonUniqueException()
    {
        $this->expectException(NonUniqueOptionsException::class);
        $optsNames = ['opt1', 'optDuplicate', 'opt3', 'optDuplicate'];
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn($optsNames);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn([]);

        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
    }


    /**
     * Test construct it must been well while contracts method
     * Rule::getRequiredOptsNames() have right set of options names.
     *
     * @dataProvider providerValidOptsWithRequired
     *
     * @param array $optsNames
     * @param array $requiredOptsNames
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::__construct()
     */
    public function testValidRequiredOptsNames(array $optsNames, array $requiredOptsNames)
    {
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();

        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn($optsNames);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn($requiredOptsNames);

        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);

        $this->assertSame($requiredOptsNames, $mock->{self::NAME_ABS_METHOD_GET_REQUIRED}());
    }


    /**
     * Provider.
     * Contain some variants with valid sets of both options.
     *
     * @return array
     *
     * @see RuleTest::testValidRequiredOptsNames()
     */
    public function providerValidOptsWithRequired():array
    {
        return [
            'empty'    => [['opt'], []],
            'oneOfTwo' => [['opt1', 'opt2'], ['opt1']],
            'twoOfTwo' => [['opt1', 'opt2'], ['opt1', 'opt2']],
        ];
    }


    /**
     * Test Exception while contracts method Rule::getRequiredOptsNames() have
     * invalid options names set. It contains duplicate.
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::__construct()
     */
    public function testInvalidGetRequiredOptsNamesNonUniqueException()
    {
        $this->expectException(NonUniqueOptionsException::class);
        $optsNames         = ['opt1', 'opt2', 'opt3', 'opt4'];
        $optsRequiredNames = ['opt1', 'opt1'];
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn($optsNames);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn($optsRequiredNames);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
    }


    /**
     * Test Exception while contracts method Rule::getRequiredOptsNames() have
     * invalid options names set. It contains options most than contains at
     * Rule::getOptsNames().
     *
     * @throws \ReflectionException
     */
    public function testInvalidGetRequiredOptsNamesOverflowException()
    {
        $this->expectException(OverflowRequiredOptionsException::class);
        $optsNames         = ['opt1'];
        $optsRequiredNames = ['opt1', 'opt5'];
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn($optsNames);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn($optsRequiredNames);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
    }


    /**
     * Test Exception while contracts method Rule::getRequiredOptsNames() have
     * invalid options names set. It contains option name what does not exist
     * at Rule::getOptsNames().
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::__construct()
     */
    public function testInvalidGetRequiredOptsNamesNonExistException()
    {
        $this->expectException(NonExistOptionException::class);
        $optsNames         = ['opt1', 'opt2', 'opt3', 'opt4'];
        $optsRequiredNames = ['opt1', 'opt5'];
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn($optsNames);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn($optsRequiredNames);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
    }


    /**
     * Test Exception while appending value to key what does not exist.
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::addOptValue()
     */
    public function testInvalidAddOptValueOptionKeyNotExistException()
    {
        $this->expectException(NonExistOptionException::class);
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn(['opt1']);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn([]);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
        $mock->addOptValue('opt2', 'de');
    }


    /**
     * Test append value to options with Rule::addOptsValues() method.
     * Check whether appended values to private properties via Reflection.
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::addOptValue()
     */
    public function testValidAddOptValue()
    {
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn(['opt1']);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn(['opt1']);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
        /* get private property of parent class */
        $property = $this->getReflectedHiddenProperty($mock);

        $mock->addOptValue('opt1', 'de');
        /* check first */
        $this->assertContains('de', $property->getValue($mock)['opt1']);
        $mock->addOptValue('opt1', 'en');
        /* check second */
        $this->assertContains('en', $property->getValue($mock)['opt1']);
        /* check if first still exist */
        $this->assertContains('de', $property->getValue($mock)['opt1']);
    }


    /**
     * Test Exception while appending value what had been already added before.
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::addOptValue()
     */
    public function testInvalidAddOptValueNonUniqueException()
    {
        $this->expectException(NonUniqueValueException::class);
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn(['opt1']);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn([]);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
        /* add valid values */
        $mock->addOptValue('opt1', 'de');
        $mock->addOptValue('opt1', 'en');
        /* add value duplicate it must throw */
        $mock->addOptValue('opt1', 'de');
    }


    /**
     * Test Exception while set up values to option key what does not exist.
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::setOptValues()
     */
    public function testInvalidSetOptValuesOptionKeyNotExistException()
    {
        $this->expectException(NonExistOptionException::class);
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn(['opt1']);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn([]);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
        $mock->setOptValues('opt2', ['de']);
    }


    /**
     * Test set up set of values to options with Rule::addOptsValues() method.
     * Check whether appended values to private properties via Reflection.
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::setOptValues()
     */
    public function testValidSetOptValues()
    {
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn(['opt1']);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn([]);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
        /* get private property of parent class */
        $property = $this->getReflectedHiddenProperty($mock);
        $mock->setOptValues('opt1', ['de', 'en', 'fr']);
        /* check all separately */
        $this->assertContains('de', $property->getValue($mock)['opt1']);
        $this->assertContains('en', $property->getValue($mock)['opt1']);
        $this->assertContains('fr', $property->getValue($mock)['opt1']);
    }


    /**
     * Test Exception while set contains duplicate value.
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::setOptValues()
     */
    public function testInvalidSetOptValuesNonUniqueException()
    {
        $this->expectException(NonUniqueValueException::class);
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn(['opt1']);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn([]);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
        $mock->setOptValues('opt1', ['de', 'en', 'de']);
    }


    /**
     * Test set up set of values to options with Rule::addOptsValues() method.
     * Check whether appended values to private properties via Reflection.
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::setOptValues()
     */
    public function testValidSetOptValuesWithOverride()
    {
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn(['opt1']);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn([]);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
        /* get private property of parent class */
        $property = $this->getReflectedHiddenProperty($mock);

        /* set first time */
        $mock->setOptValues('opt1', ['de', 'en']);
        /* check all separately and not set before */
        $this->assertContains('de', $property->getValue($mock)['opt1']);
        $this->assertContains('en', $property->getValue($mock)['opt1']);
        $this->assertNotContains('fr', $property->getValue($mock)['opt1']);
        $this->assertNotContains('ru', $property->getValue($mock)['opt1']);

        /* set second time, override and check again */
        $mock->setOptValues('opt1', ['fr', 'ru']);
        /* check all separately again new and and not exist by first time */
        $this->assertNotContains('de', $property->getValue($mock)['opt1']);
        $this->assertNotContains('en', $property->getValue($mock)['opt1']);
        $this->assertContains('fr', $property->getValue($mock)['opt1']);
        $this->assertContains('ru', $property->getValue($mock)['opt1']);
    }


    /**
     * Test Exception which fired when start using match but not all required
     * opts had been set before.
     *
     * It checks via Reflection and call protected method what calls each times
     * while Rule::match() executing.
     *
     * @dataProvider providerNotSetRequiredException
     *
     * @param array $optsNames
     * @param array $optsRequiredNames
     * @param array $set
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::match()
     */
    public function testInvalidSetOptsValuesNotExistRequiredExceptionReflection(
        array $optsNames,
        array $optsRequiredNames,
        array $set
    ) {
        $this->expectException(RequiredOptionsDoesNotSetException::class);
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn($optsNames);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn($optsRequiredNames);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
        foreach ($set as $optName => $values) {
            $mock->setOptValues($optName, $values);
        }

        /* invoke protected method for check is all opt filled */
        $this->invokeMethod($mock, self::NAME_METHOD_CHECK_COMPLETED);
    }



    /**
     * Test Exception which fired when start using match but not all required
     * opts had been set before.
     *
     * It checks via origin method Rule::match() and provide empty array.
     *
     * Exception what testing there is must thrown early than will be check
     * provided data to Rule::match().
     *
     * @dataProvider providerNotSetRequiredException
     *
     * @param array $optsNames
     * @param array $optsRequiredNames
     * @param array $set
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::match()
     */
    public function testInvalidSetOptValuesNotExistRequiredException(
        array $optsNames,
        array $optsRequiredNames,
        array $set
    ) {
        $this->expectException(RequiredOptionsDoesNotSetException::class);
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn($optsNames);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn($optsRequiredNames);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
        foreach ($set as $optName => $values) {
            $mock->setOptValues($optName, $values);
        }
        /* that method call method for check before start compare */
        $mock->match([]);
    }


    /**
     * Provider.
     * Contain $optsNames, $optsRequiredNames, $setOptsValues, $compareValues.
     *
     * @return array
     *
     * @see RuleTest::testInvalidSetOptsValuesNotExistRequiredExceptionReflection()
     * @see RuleTest::testInvalidSetOptValuesNotExistRequiredException()
     */
    public function providerNotSetRequiredException():array
    {
        return [
            '1Opt1Req'        => [['opt'], ['opt'], []],
            '2Opts1Req'       => [['opt1', 'opt2'], ['opt1'], []],
            '2Opts2Req1Value' => [['opt1', 'opt2'], ['opt1', 'opt2'], ['opt1' => ['val']]],
            '3Opts2Req1Value' => [
                ['opt1', 'opt2', 'opt3'],
                ['opt1', 'opt2'],
                ['opt2' => ['val']]
            ],
            '3Opts3Req2Value' => [
                ['opt1', 'opt2', 'opt3'],
                ['opt1', 'opt2', 'opt3'],
                ['opt1' => ['val'], 'opt3' => ['val']],
            ],
        ];
    }


    /**
     * Tests Exception what will be thrown while provided invalid
     * data structure.
     *
     * It can be:
     *
     * ```
     *  *) Not all existing opts names.
     *  *) Overflow opts names.
     *  *) Misspell opts names.
     *  *) Invalid value. Now deprecated type is Array.
     * ```
     *
     * @dataProvider providerInvalidOptionsWithCompare
     *
     * @param array  $optsNames
     * @param array  $optsRequiredNames
     * @param array  $setOpts
     * @param array  $compareValues
     * @param string $exception
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::match()
     */
    public function testInvalidProviderToMatchExceptions(
        array $optsNames,
        array $optsRequiredNames,
        array $setOpts,
        array $compareValues,
        string $exception
    ) {
        $this->expectException($exception);
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn($optsNames);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn($optsRequiredNames);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
        foreach ($setOpts as $optName => $optValues) {
            $mock->setOptValues($optName, $optValues);
        }

        $mock->match($compareValues);
    }


    /**
     * Provider.
     * Contain $optsNames, $optsRequiredNames, $setOptsValues, $compareValues.
     *
     * @return array
     *
     * @see RuleTest::testInvalidProviderToMatchExceptions()
     */
    public function providerInvalidOptionsWithCompare():array
    {
        return [
            'notFull' => [
                ['opt1', 'opt2'],
                ['opt1'],
                ['opt1' => ['val1', 'val2'], 'opt2' => []],
                ['opt1' => 'val2'],
                InvalidProvidedOptionsException::class
            ],
            'overflow' => [
                ['opt1', 'opt2'],
                ['opt1'],
                ['opt1' => ['val1', 'val2'], 'opt2' => []],
                ['opt1' => 'val2', 'opt2' => 3, 'opt3' => 'extra'],
                InvalidProvidedOptionsException::class
            ],
            'notExist' => [
                ['opt1', 'opt2'],
                ['opt1'],
                ['opt1' => ['val1', 'val2'], 'opt2' => []],
                ['opt1' => 'val2', 'opt3' => 3],
                InvalidProvidedOptionsException::class
            ],
            'invalidValue' => [
                ['opt1', 'opt2'],
                ['opt1'],
                ['opt1' => ['val1', 'val2'], 'opt2' => []],
                ['opt1' => 'val2', 'opt2' => []],
                InvalidValueException::class
            ]
        ];
    }


    /**
     * Method check valid result of Rule::match().
     *
     * @dataProvider providerSetForMatching
     *
     * @param array   $optsNames
     * @param array   $optsRequiredNames
     * @param array   $setOptsValues
     * @param array   $compareValues
     * @param boolean $expect
     *
     * @throws \ReflectionException
     *
     * @covers \Planet17\RulesMapResolver\Rule::match()
     */
    public function testMatching(
        array $optsNames,
        array $optsRequiredNames,
        array $setOptsValues,
        array $compareValues,
        int $expect
    ) {
        /** @var RuleContract|MockObject $mock */
        $mock = $this->getMockRuleWithoutConstructor();
        $mock->method(self::NAME_ABS_METHOD_GET_OPTS)->willReturn($optsNames);
        $mock->method(self::NAME_ABS_METHOD_GET_REQUIRED)->willReturn($optsRequiredNames);
        /* replace and run __construct feature */
        $this->fixtureReflectionInvokeConstructRuleClass($mock);
        foreach ($setOptsValues as $optName => $optValues) {
            $mock->setOptValues($optName, $optValues);
        }
        $this->assertSame($expect, $mock->match($compareValues));
    }


    /**
     * Provider.
     * Contain $optsNames, $optsRequiredNames, $setOptsValues, $compareValues,
     * and $expect for matching.
     *
     * @return array
     *
     * @see RuleTest::testMatching()
     */
    public function providerSetForMatching():array
    {
        return [
            'oneOptionFullMatch'   => [
                ['opt1'],
                [],
                ['opt1' => ['val7']],
                ['opt1' => 'val7'],
                1,
            ],
            'oneOptionMaskMatch'   => [
                ['opt1'],
                [],
                [],
                ['opt1' => 'val7'],
                1
            ],
            'oneOptionNoMatch'     => [
                ['opt1'],
                ['opt1'],
                ['opt1' => ['val7']],
                ['opt1' => 'val3'],
                0
            ],
            'twoOptionFullMatch'   => [
                ['opt1', 'opt2'],
                ['opt2'],
                ['opt1' => ['val7'], 'opt2' => ['val7', 'val8']],
                ['opt1' => 'val7', 'opt2' => 'val8'],
                1
            ],
            'twoOptionMaskMatch'   => [
                ['opt1', 'opt2'],
                ['opt2'],
                ['opt1' => [], 'opt2' => ['val7', 'val8']],
                ['opt1' => 'val3', 'opt2' => 'val8'],
                1
            ],
            'twoOptionNoMatch'     => [
                ['opt1', 'opt2'],
                ['opt2'],
                ['opt1' => [], 'opt2' => ['val7', 'val9']],
                ['opt1' => 'val3', 'opt2' => 'val8'],
                0
            ],
            'threeOptionFullMatch' => [
                ['opt1', 'opt2', 'opt3'],
                [],
                ['opt1' => ['val'], 'opt2' => ['val7', 'val9'], 'opt3' => ['val3']],
                ['opt1' => 'val', 'opt2' => 'val9', 'opt3' => 'val3'],
                1
            ],
            'threeOptionMaskMatch' => [
                ['opt1', 'opt2', 'opt3'],
                [],
                ['opt1' => [], 'opt2' => [], 'opt3' => ['val3']],
                ['opt1' => 'val', 'opt2' => 'val9', 'opt3' => 'val3'],
                1
            ],
            'threeOptionNoMatch'   => [
                ['opt1', 'opt2', 'opt3'],
                [],
                ['opt1' => [], 'opt2' => ['val'], 'opt3' => ['val3']],
                ['opt1' => 'val', 'opt2' => 'val9', 'opt3' => 'val3'],
                0
            ]
        ];
    }


    /**
     * Get Mock for Rule::class with disabled __construct().
     *
     * @return MockObject
     */
    protected function getMockRuleWithoutConstructor():MockObject
    {
        return $this->getMockForAbstractClass(
            Rule::class,
            [],
            '',
            false,
            false,
            true,
            [self::NAME_ABS_METHOD_GET_OPTS, self::NAME_ABS_METHOD_GET_REQUIRED]
        );
    }


    /**
     * Method encapsulate  implementation for running methods at Mock from disabled Rule::_construct
     * method what contained almost at every test.
     *
     * @param MockObject $mock
     *
     * @throws \ReflectionException
     */
    protected function fixtureReflectionInvokeConstructRuleClass(MockObject $mock)
    {
        $this->invokeMethod($mock, 'isValidPresetOpts');
        $this->invokeMethod($mock, 'initPresetOpts');
    }


    /**
     * Method encapsulate invoke protected method.
     *
     * @param MockObject $mock
     * @param string     $methodName
     *
     * @throws \ReflectionException
     */
    protected function invokeMethod(MockObject $mock, string $methodName)
    {
        $method = new \ReflectionMethod($mock, $methodName);
        $method->setAccessible(true);
        $method->invoke($mock);
    }


    /**
     * Method encapsulate extracting hidden property by Reflection.
     *
     * @param MockObject $object
     *
     * @return \ReflectionProperty
     * @throws \ReflectionException
     */
    protected function getReflectedHiddenProperty(MockObject $object):\ReflectionProperty
    {
        $property = (new \ReflectionClass($object))->getParentClass()->getProperty('opts');
        $property->setAccessible(true);
        return $property;
    }
}
